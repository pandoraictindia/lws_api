<?php
//require(APPPATH.'models/Go_model.php');
class Sample_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function insertdata($data,$table,&$errormessage)
	{
		$result = 0;
		$result = $this->db->insert($table,$data);
		if($result == 1)
		{
			$result = $this->db->insert_id();
		}
		else
		{
			$errormessage = "Record not inserted.";
		}
		
		return $result;
	}

	public function updateRecord($data,$record_id,$table,&$errormessage)
	{
		$result = 0;
		$this->db->where('id',$record_id);
		$result = $this->db->update($table,$data);
		if($result !== 1)
		{
			$errormessage = "Something wrong.";
		}
		
		return $result;
	}

	public function deleteRecord($record_id,$table,&$errormessage)
	{
		$result = 0;
		$this->db->where('id',$record_id);
		$result = $this->db->update($table,array('active'=>'0'));		
		if($result !== 1)
		{
			$errormessage = "Record not deleted.";
		}
		
		return $result;
	}

	public function checkLogin($data,&$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,type');
		$this->db->from('user');
		$this->db->where('email',$data['email']);
		$this->db->where('password',$data['password']);
		$this->db->where('active','1');
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = 'Wrong email or password.';
		}
		
		return $result;
	}	

	public function getConsultant($data, &$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,phone,address,contact_person,secondary_email')	;
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','2');
		//$this->db->where('created_by',$data['user_id']);
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function emailExists($email, &$errormessage)
	{
		$result = 0;  //  email exists
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email',$email); 
		$this->db->where('active','1');		
		$res = $this->db->get()->row_array();
		if(empty($res))
		{
			$result = 1;   // email not exists
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function updateemailExists($email, $record_id,&$errormessage)
	{
		$result = 0;  //  email exists
		$query = $this->db->query("SELECT * from user WHERE email='$email' AND active='1' AND NOT(id='$record_id') ");				
		$res = $query->row_array();
		if(empty($res))
		{
			$result = 1;   // email not exists
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function emailavailable($email, &$errormessage)
	{
		$result = array();  
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email',$email); 
		$this->db->where('active','1');		
		$result = $this->db->get()->row_array();
		if(empty($res))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getClient($data, &$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,phone,address,contact_person,servicing_frequency,logo_image,secondary_email')	;
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','3');
		$this->db->where('created_by',$data['user_id']);
		if($data['begin'] !== '' )
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getEngineer($data, &$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,phone,address,contact_person')	;
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','4');
		$this->db->where('created_by',$data['user_id']);
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('id','DESC');		
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	// upload  excel	
	public function UploadExcel($data, &$errormessage)
	{
		
		//'expiry_date' =>date("d/Y", strtotime("+5 year",strtotime('01/'.$data['list'][0][8]))),
		$result = 0;
		//check sr no exists
		$this->db->select('id,nfc_id');
		$this->db->from('fire_extinguisher');
		$this->db->where('fe_no',$data['list'][0][0]);
		$this->db->where('client_id',$data['client_id']);
		$this->db->where('active','1');
		$exists = $this->db->get()->row_array();
		if(empty($exists))
		{	
			$insertArr = array(
		  					//'sr_no' => $data['sr_no'],
		  					'fe_no' => $data['list'][0][0],
		  					'location' => $data['list'][0][1],
		  					'type' => $data['list'][0][2],
		  					'ct_sp' => $data['list'][0][3],
		  					'class' => $data['list'][0][4],
		  					'capacity' => $data['list'][0][5],
		  					'ctt' => $data['list'][0][6],
		  					'cta' => $data['list'][0][7],				  	
		  					'make' => $data['list'][0][8],				
		  					'serial' => $data['list'][0][9],
		  					'mfr' => $data['list'][0][10],
		  					'expiry_date' =>date("m-Y", strtotime('+'.$data['expiry'],strtotime('01-'.$data['list'][0][10]))),
		  					'hpt_date' => $data['hpt_date'],
		  					'htp_due' => $data['hpt_due'],
		  					'servicing_date' => $data['servicing_date'],
		  					// 'servicing_due' => $data['servicing_due'],
		  					'refill_date' => $data['refill_date'],
		  					'refill_due' => $data['refill_due'],
		  					'user_id' => $data['user_id'],
		  					'consultant_id' => $data['consultant_id'],
		  					'client_id' => $data['client_id'],
		  					'nfc_id' => $data['nfc_id']
		  				);			
			$result = $this->db->insert('fire_extinguisher',$insertArr);
			
			$this->db->insert('servicing_history',$insertArr);
		}
		else
		{
			$insertArr = array(
		  					'location' => $data['list'][0][1],
		  					'type' => $data['list'][0][2],
		  					'ct_sp' => $data['list'][0][3],
		  					'class' => $data['list'][0][4],
		  					'capacity' => $data['list'][0][5],
		  					'ctt' => $data['list'][0][6],
		  					'cta' => $data['list'][0][7],				  	
		  					'make' => $data['list'][0][8],				
		  					'serial' => $data['list'][0][9],
		  					'mfr' => $data['list'][0][10],
		  					'expiry_date' =>date("m-Y", strtotime('+'.$data['expiry'],strtotime('01-'.$data['list'][0][10]))),
		  					'hpt_date' => $data['hpt_date'],
		  					'htp_due' => $data['hpt_due'],
		  					'servicing_date' => $data['servicing_date'],
		  					// 'servicing_due' => $data['servicing_due'],
		  					'refill_date' => $data['refill_date'],
		  					'refill_due' => $data['refill_due'],
		  					'consultant_id' => $data['consultant_id'],
		  					'client_id' => $data['client_id'],
		  				);
		  	$this->db->where('id',$exists['id']);
		  	$result = $this->db->update('fire_extinguisher',$insertArr);
		  	// update in history
		  	// $this->db->where('nfc_id',$exists['nfc_id']);
		  	// $result = $this->db->update('fire_extinguisher',$insertArr);
		}			
		if($result == 0)
		{
			$errormessage = "Some unknown error has occurred. Please try again.";					 
		}				
		return $result;	
	}

	public function getAdminReport($data, &$errormessage)
	{
		$result = array();
		strlen($data['month']) == 1 ? $data['month'] = '0'.$data['month'] : '';
		$date = $data['month'].'/'.$data['year'];
		$this->db->select('*')	;
		$this->db->from('fire_extinguisher');
		$this->db->where('active','1');
		$this->db->where('MONTH(servicing_date)', $data['month']);
		$this->db->where('YEAR(servicing_date)', $data['year']);
		//$this->db->where('mfr',$date);
		// if($data['admin_id'] !== '')
		// {
		// 	$this->db->where('user_id',$data['admin_id']);
		// }
		if($data['consultant_id'] !== '')
		{
			$this->db->where('consultant_id',$data['consultant_id']);
		}
		if($data['client_id'] !== '')
		{
			$this->db->where('client_id',$data['client_id']);
		}
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('fe_no','ASC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getMonthlyReport($data, &$errormessage)
	{
		$result = array();
		$this->db->select('*')	;
		$this->db->from('servicing_history');
		$this->db->where('active','1');
		$this->db->where('MONTH(servicing_date)', $data['month']);
		$this->db->where('YEAR(servicing_date)', $data['year']);
		
		if($data['consultant_id'] !== '')
		{
			$this->db->where('consultant_id',$data['consultant_id']);
		}
		if($data['client_id'] !== '')
		{
			$this->db->where('client_id',$data['client_id']);
		}
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('fe_no','ASC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}


	public function getRepresentativeName($id,&$errormessage)
	{
		$result = array();
		$this->db->select('name');
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('id',$id);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}	

	public function updateFireExtinguisher($data,$editid, &$errormessage)
	{
		$result = 0;		
		$this->db->where('id',$editid);
		$result = $this->db->update('fire_extinguisher',$data);		
		if($result == 0)
		{
			$errormessage = "Records not updated.";
		}
		return $result;
	}

	

	public function getExpiryTime(&$errormessage)
	{
		$result = array();
		$this->db->select('expiry');
		$this->db->from('time_master');
		$this->db->where('id','1');
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}

	

	public function getNFCTagReport($data, &$errormessage)
	{
		$result = array();
		$this->db->select('a.sr_no,a.fe_no,a.nfc_id,a.location,a.type,a.class,a.capacity,a.ct_sp,b.name as co_name');
		$this->db->from('fire_extinguisher as a');
		$this->db->join('user as b','a.client_id=b.id');
		$this->db->where('a.active','1');
		$this->db->where('a.user_id',$data['admin_id']);
		$this->db->where('a.consultant_id',$data['consultant_id']);
		$this->db->where('a.client_id',$data['client_id']);
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		//$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getHistoryReport($data, &$errormessage)
	{
		$result = array();
		$this->db->select('*');
		$this->db->from('history');
		$this->db->where('active','1');
		$this->db->where('fe_no',$data['fe_no']);
		$this->db->where('client_id',$data['client_id']);
		$this->db->where('YEAR(date)', $data['year']);		
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getFEDetail($data, &$errormessage)
	{
		$result = array();
		$this->db->select('id,fe_no,location,type,ct_sp,class,capacity');
		$this->db->from('fire_extinguisher');
		$this->db->where('active','1');
		$this->db->where('fe_no',$data['fe_no']);
		$this->db->where('client_id',$data['client_id']);		
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getLastSrno($client_id,&$errormessage)
	{
		$sr_no = 0;
		$this->db->select('sr_no');
		$this->db->from('fire_extinguisher');
		$this->db->where('client_id',$client_id);
		$this->db->order_by('sr_no','DESC');
		$result = $this->db->get()->row_array();
		if(!empty($result))
		{
			$sr_no = $result['sr_no'];
		}
		else
		{
			$errormessage = "Record not available.";
		}
		return $sr_no + 1;
	}

	public function changePassword($data, &$errormessage)
	{
		$result = 0;
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('id',$data['user_id']);
		$this->db->where('password',md5($data['oldpass']));
		$this->db->where('active','1');
		$exists = $this->db->get()->row_array();
		if(empty($exists))
		{
			$errormessage = "Old password not match.";
		}
		else
		{
			$this->db->where('id',$data['user_id']);
			$result = $this->db->update('user',array('password' => md5($data['newpass'])));
			if($result !== 1)
			{
				$errormessage = "Password not changed. Please try again.";
			}
		}

		return $result;
	}

	public function getexpirynotifications(&$errormessage)
	{
		$result = 1;
		$currentmonth = date('m/Y');

		//get clients
		$this->db->select('id,name,email')	;
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','3');
		$clients = $this->db->get()->result_array();
		if(!empty($clients))
		{
			$notifications = array();
			for($i=0;$i<count($clients);$i++)
			{
				//get expire fire extinguishers of client
				$this->db->select('*')	;
				$this->db->from('fire_extinguisher');
				$this->db->where('active','1');
				$this->db->where('client_id',$clients[$i]['id']);
				$tags = $this->db->get()->result_array();
				if(!empty($tags))
				{
					for($j=0;$j<count($tags);$j++)
					{
						//echo $tags[$j]['expiry_date'].'---';
						//echo $currentmonth.'..';
						//check expiry date
						if($tag[$j]['expiry_date'] == $currentmonth)
						{
							array_push($notifications, $tag[$j]); 
							
						}
						else
						{
							//get dues period
							$exp_time = $this->getDuePeriod($tag[$j]['type'],$errormessage);
							//check refill due
							//get refill due date
							if($tag[$j]['refill_date'] !== NULL && $tag[$j]['refill_date'] !== '' && $tag[$j]['refill_date'] !== 'XX')
							{ 
								$tag[$j]['refill_due'] = date("m/Y", strtotime("+".$exp_time['refill'], strtotime($tag[$j]['refill_date'])));
								if($tag[$j]['refill_due'] == $currentmonth)
								{
									array_push($notifications, $tag[$j]); 
									
								}
												
							}
							else
							{
								//get hpt due date
								if($tag[$j]['hpt_date'] !== NULL && $tag[$j]['hpt_date'] !== '' && $tag[$j]['hpt_date'] !== 'XX')
								{ 
									$tag[$j]['hpt_due'] = date("m/Y", strtotime("+".$exp_time['hpt'], strtotime($tag[$j]['hpt_date'])));
									if($tag[$j]['hpt_due'] == $currentmonth)
									{
										array_push($notifications, $tag[$j]); 
										
									}
													
								}
							}

						}

						//set emaildata
						$html = '<p>Hello,'.$clients[$i]['name'].'</p>';
						$html .= '<p>report</p>';

						$emaildata['to'] = $clients[$i]['email'];
						$emaildata['html'] = $html;
							
						$this->sendmail($emaildata);
					}					
				}
			}
		}

		return $result;
	}

	public function getServicingFrequency($client_id,&$errormessage)
	{
		$result = array();
		$this->db->select('servicing_frequency');
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('id',$client_id);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}

	public function getDuePeriod($type,$ctsp,&$errormessage)
	{
		$result = array();
		$this->db->select('hpt,refill,life');
		$this->db->from('schedule');
		$this->db->where('type',trim($type));
		$this->db->where('ct_sp',trim($ctsp));
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}

	public function updatepassword($user_id,$password,&$errormessage)
	{
		$result = array();
		$this->db->where('id',$user_id);
		$this->db->update('ct_sp',trim($ctsp));
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}


	public function sendmail($data)
	{
		$config = array(
		   'protocol'  => 'smtp',
		   'smtp_host' => 'ssl://smtp.googlemail.com',
		   'smtp_port' => 465,
		   'smtp_user' => 'siddhitest12@gmail.com',
		   'smtp_pass' => 'siddhi@123',
		   'mailtype'  => 'html',
		   'charset' => 'iso-8859-1',
		   'wordwrap' => TRUE
   		);

		$this->load->library('email');
   		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		
		$this->email->to($data['to']);
		$this->email->from('siddhitest12@gmail.com','LWS-TAG');
		$this->email->subject('Notification For Fire Extinguisher');
		$this->email->message($data['html']);		
		//Send email
		//$this->email->send();
		print_r($data);
	}

	public function getAuditor($data, &$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,phone,designation,consultant');
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','5');
		
		if($data['begin'] !== '')
		{
			$this->db->offset($data['begin']);
			$this->db->limit(10);
		}
		$this->db->order_by('id','DESC');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}




}