<?php
class Cronjob_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function getConsultantRemider($logo_url,&$errormessage)
	{
		//get all client
		$query = $this->db->query("SELECT a.id,a.name,a.email,b.name as consultant_name,b.email as consultant_email, a.secondary_email as client_secondary_email FROM `user` a INNER JOIN `user` b ON a.created_by = b.id WHERE a.type='3' AND a.active='1' ");
		$client = $query->result_array();
        
		
		if(!empty($client))
		{
		    //echo 'df';exit;
			for($i=0;$i<count($client);$i++)
			{
				$client_id = $client[$i]['id'];
				
				$query1 = $this->db->query("SELECT fe_no,type,class,ct_sp,capacity,'expiry' AS `due_type`   FROM `fire_extinguisher` WHERE SUBSTRING(expiry_date,1,2) = MONTH(CURRENT_DATE()) AND SUBSTRING(expiry_date,4,4) = YEAR(CURRENT_DATE()) AND client_id=".$client_id."
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'refill' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(refill_due) = MONTH(CURRENT_DATE()) AND YEAR(refill_due) = YEAR(CURRENT_DATE())
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'hpt' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(htp_due) = MONTH(CURRENT_DATE()) AND YEAR(htp_due) = YEAR(CURRENT_DATE()) ");	

				$Month1Data = $query1->result_array();
				// print_r($Month1Data);
				
				//get dues dates of +1 month
				$query2 = $this->db->query("SELECT fe_no,type,class,ct_sp,capacity, 'expiry' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND SUBSTRING(expiry_date,1,2) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH)) AND SUBSTRING(expiry_date,4,4) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH))
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'refill' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(refill_due) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH)) AND YEAR(refill_due) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH))
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'hpt' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(htp_due) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH)) AND YEAR(htp_due) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +1 MONTH))");				
				$Month2Data = $query2->result_array();
                //print_r($Month2Data);
                
                
                $query3 = $this->db->query("
					SELECT fe_no,type,class,ct_sp,capacity,'expiry' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND SUBSTRING(expiry_date,1,2) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH)) AND SUBSTRING(expiry_date,4,4) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH))
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'refill' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(refill_due) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH)) AND YEAR(refill_due) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH))
					UNION
					SELECT fe_no,type,class,ct_sp,capacity,'hpt' AS `due_type` FROM `fire_extinguisher`WHERE active='1' AND client_id=".$client_id." AND MONTH(htp_due) = MONTH(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH)) AND YEAR(htp_due) = YEAR(DATE_ADD(CURRENT_DATE(), INTERVAL +2 MONTH))");				
				$Month3Data = $query3->result_array();
				//print_r($Month3Data);
				
				$expiry_due1 = [];
				$refill_due1 = [];
				$hpt_due1 = [];
				$expiry_due2 = [];
				$refill_due2 = [];
				$hpt_due2 = [];
				$expiry_due3= [];
				$refill_due3= [];
				$hpt_due3= [];

				$month1 = [];

				$month2 = [];

				$month3 = [];
				
				if(!empty($Month1Data))
				{					
					for($j=0;$j<count($Month1Data);$j++)
					{
						// if array empty
						if(empty($month1)) {
							$temp = [
										'type'=>$Month1Data[$j]['type'],
										'ct_sp'=>$Month1Data[$j]['ct_sp'],
										'class'=>$Month1Data[$j]['class'],
										'capacity'=>$Month1Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];
									if($Month1Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month1Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month1Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
				 					array_push($month1, $temp);
						}
						else {
							$matchFound = 0;
							//if array have values
				 			for($m=0;$m<count($month1);$m++) {
				 				
								if($Month1Data[$j]['type'] == $month1[$m]['type'] && $Month1Data[$j]['ct_sp'] == $month1[$m]['ct_sp'] && $Month1Data[$j]['class'] == $month1[$m]['class'] && $Month1Data[$j]['capacity'] == $month1[$m]['capacity']) {
									$matchFound = 1;
								    
									// if same entry available
									if($Month1Data[$j]['due_type'] == 'expiry') {
										if($month1[$m]['expiry_srno'] == '') {
											$month1[$m]['expiry_srno'] = $Month1Data[$j]['fe_no'];
										} else {
											$month1[$m]['expiry_srno'] .=  ','.$Month1Data[$j]['fe_no'];
										}
										$month1[$m]['expiry_count'] =  $month1[$m]['expiry_count'] + 1;
									}

									if($Month1Data[$j]['due_type'] == 'refill') {
										if($month1[$m]['refill_srno'] == '') {
											$month1[$m]['refill_srno'] = $Month1Data[$j]['fe_no'];
										} else {
											$month1[$m]['refill_srno'] .=  ','.$Month1Data[$j]['fe_no'];
										}
										$month1[$m]['refill_count'] =  $month1[$m]['refill_count'] + 1;
									}

									if($Month1Data[$j]['due_type'] == 'hpt') {
										if($month1[$m]['hpt_srno'] == '') {
											$month1[$m]['hpt_srno'] = $Month1Data[$j]['fe_no'];
										} else {
											$month1[$m]['hpt_srno'] .=  ','.$Month1Data[$j]['fe_no'];
										}
										$month1[$m]['hpt_count'] =  $month1[$m]['hpt_count'] + 1;
									}
								}
				 			}

				 			if($matchFound = 0) {
				 				//same entry not available
									// $temp = [
									// 	'type'=>$Month1Data['type'],
									// 	'ct_sp'=>$Month1Data['ct_sp'],
									// 	'class'=>$Month1Data['class'],
									// 	'capacity'=>$Month1Data['capacity'],
									// 	'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
									// 	'expiry_srno'=>'', 'expiry_count'=>0
									// ];
				 				$temp = [
										'type'=>$Month1Data[$j]['type'],
										'ct_sp'=>$Month1Data[$j]['ct_sp'],
										'class'=>$Month1Data[$j]['class'],
										'capacity'=>$Month1Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];
									if($Month1Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month1Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month1Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month1Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
									array_push($month1, $temp);
				 			}
				 		}
					}
				}
				
				
				
				if(!empty($Month2Data))
				{				
					for($j=0;$j<count($Month2Data);$j++)
					{
						// if array empty
						if(empty($month2)) {
							$temp = [
										'type'=>$Month2Data[$j]['type'],
										'ct_sp'=>$Month2Data[$j]['ct_sp'],
										'class'=>$Month2Data[$j]['class'],
										'capacity'=>$Month2Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];
									if($Month2Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month2Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month2Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
									array_push($month2, $temp);
						}
						else {
							$matchFound1 = 0;
							//if array have values
				            for($m=0;$m<count($month2);$m++) {
				    			if($Month2Data[$j]['type'] == $month2[$m]['type'] && $Month2Data[$j]['ct_sp'] == $month2[$m]['ct_sp'] && $Month2Data[$j]['class'] == $month2[$m]['class'] && $Month2Data[$j]['capacity'] == $month2[$m]['capacity']) {
				    					$matchFound1 = 1;
				    			        // if same entry available
    									if($Month2Data[$j]['due_type'] == 'expiry') {
    										if($month2[$m]['expiry_srno'] == '')     {
    											$month2[$m]['expiry_srno'] = $Month2Data[$j]['fe_no'];
    										} else {
    											$month2[$m]['expiry_srno'] .=  ','.$Month2Data[$j]['fe_no'];
    										}
    										$month2[$m]['expiry_count'] =  $month2[$m]['expiry_count'] + 1;
    									}
    
    									if($Month2Data[$j]['due_type'] == 'refill') {
    										if($month2[$m]['refill_srno'] == '') {
    											$month2[$m]['refill_srno'] = $Month2Data[$j]['fe_no'];
    										} else {
    											$month2[$m]['refill_srno'] .=  ','.$Month2Data[$j]['fe_no'];
    										}
    										$month2[$m]['refill_count'] =  $month2[$m]['refill_count'] + 1;
    									}
    
    									if($Month2Data[$j]['due_type'] == 'hpt') {
    										if($month2[$m]['hpt_srno'] == '') {
    											$month2[$m]['hpt_srno'] = $Month2Data[$j]['fe_no'];
    										} else {
    											$month2[$m]['hpt_srno'] .=  ','.$Month2Data[$j]['fe_no'];
    										}
    										$month2[$m]['hpt_count'] =  $month2[$m]['hpt_count'] + 1;
    									}
				 				}                                
				 			}

				 			if($matchFound1 = 0) {
				 				//same entry not available
									// $temp = [
									// 	'type'=>$Month2Data['type'],
									// 	'ct_sp'=>$Month2Data['ct_sp'],
									// 	'class'=>$Month2Data['class'],
									// 	'capacity'=>$Month2Data['capacity'],
									// 	'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
									// 	'expiry_srno'=>'', 'expiry_count'=>0
									// ];
				 				$temp = [
										'type'=>$Month2Data[$j]['type'],
										'ct_sp'=>$Month2Data[$j]['ct_sp'],
										'class'=>$Month2Data[$j]['class'],
										'capacity'=>$Month2Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];
										
                                  if($Month2Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month2Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month2Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month2Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
									array_push($month2, $temp);
				 			}
						}
					}
				}


				if(!empty($Month3Data))
				{					
					for($j=0;$j<count($Month3Data);$j++)
					{
						// if array empty
						if(empty($month3)) {
							$temp = [
										'type'=>$Month3Data[$j]['type'],
										'ct_sp'=>$Month3Data[$j]['ct_sp'],
										'class'=>$Month3Data[$j]['class'],
										'capacity'=>$Month3Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];
									if($Month3Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month3Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month3Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
									array_push($month3, $temp);
						}
						else {
							$matchFound2 = 0;
 							//if array have values
							for($m=0;$m<count($month3);$m++) {
								if($Month3Data[$j]['type'] == $month3[$m]['type'] && $Month3Data[$j]['ct_sp'] == $month3[$m]['ct_sp'] && $Month3Data[$j]['class'] == $month3[$m]['class'] && $Month3Data[$j]['capacity'] == $month3[$m]['capacity']) {
									$matchFound2 = 1;
									// if same entry available
									if($Month3Data[$j]['due_type'] == 'expiry') {
										if($month3[$m]['expiry_srno'] == '') {
											$month3[$m]['expiry_srno'] = $Month3Data[$j]['fe_no'];
										} else {
											$month3[$m]['expiry_srno'] .=  ','.$Month3Data[$j]['fe_no'];
										}
										$month3[$m]['expiry_count'] =  $month3[$m]['expiry_count'] + 1;
									}

									if($Month3Data[$j]['due_type'] == 'refill') {
										if($month3[$m]['refill_srno'] == '') {
											$month3[$m]['refill_srno'] = $Month3Data[$j]['fe_no'];
										} else {
											$month3[$m]['refill_srno'] .=  ','.$Month3Data[$j]['fe_no'];
										}
										$month3[$m]['refill_count'] =  $month3[$m]['refill_count'] + 1;
									}

									if($Month3Data[$j]['due_type'] == 'hpt') {
										if($month3[$m]['hpt_srno'] == '') {
											$month3[$m]['hpt_srno'] = $Month3Data[$j]['fe_no'];
										} else {
											$month3[$m]['hpt_srno'] .=  ','.$Month3Data[$j]['fe_no'];
										}
										$month3[$m]['hpt_count'] =  $month3[$m]['hpt_count'] + 1;
									}
								}
							}

							if($matchFound2 == 0) {
								//same entry not available
									// $temp = [
									// 	'type'=>$Month3Data['type'],
									// 	'ct_sp'=>$Month3Data['ct_sp'],
									// 	'class'=>$Month3Data['class'],
									// 	'capacity'=>$Month3Data['capacity'],
									// 	'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
									// 	'expiry_srno'=>'', 'expiry_count'=>0
									// ];

									$temp = [
										'type'=>$Month3Data[$j]['type'],
										'ct_sp'=>$Month3Data[$j]['ct_sp'],
										'class'=>$Month3Data[$j]['class'],
										'capacity'=>$Month3Data[$j]['capacity'],
										'refill_srno'=>'', 'refill_count'=>0,'hpt_srno'=>'', 'hpt_count'=>0,
										'expiry_srno'=>'', 'expiry_count'=>0
									];

									if($Month3Data[$j]['due_type'] == 'expiry') {
										$temp['expiry_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['expiry_count'] =  1;
									}

									if($Month3Data[$j]['due_type'] == 'refill') {
										$temp['refill_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['refill_count'] =  1;
									}

									if($Month3Data[$j]['due_type'] == 'hpt') {
										$temp['hpt_srno'] =  $Month3Data[$j]['fe_no'];
										$temp['hpt_count'] =  1;
									}
									array_push($month3, $temp);
							}
						}
					}
				}
				

				//set html data
				$html_data['detail']['logo_url'] = $logo_url;
				$html_data['detail'] = $client[$i];

				$html_data['detail']['month1'] = $month1;
				$html_data['detail']['month2'] = $month2;
				$html_data['detail']['month3'] = $month3;

				$html = $this->load->view('due_email',$html_data, true);
				// if($i==3)
				// {
				// echo $html;}
			    //send mail
			    if($client[$i]['client_secondary_email'] !== '' && $client[$i]['client_secondary_email']!== null) {
			    	$cc = array(trim($client[$i]['consultant_email']),trim($client[$i]['client_secondary_email']));
			    } else {
			    	$cc=trim($client[$i]['consultant_email']);
			    }

				$emaildata['to'] = trim($client[$i]['email']);
				$emaildata['subject'] = 'Due Dates';
				$emaildata['title'] = 'Due Dates';
				$emaildata['cc'] = $cc;
				$emaildata['html'] = $html;
				//print_r($emaildata);
				$this->sendEmail($emaildata);
			}
					
		}
		else
		{
			$errormessage = 'No clients available.';
		}
		return 1;

	}

	public function sendEmail($data)
	{
		$this->load->library('email');
		$this->email->initialize(PageBase::$config);
		// $this->email->set_mailtype("tls");
		$this->email->set_newline("\r\n");
		$this->email->to($data['to']);
		$this->email->from('info@livewireservices.co.in',$data['title']);
		$this->email->cc($data['cc']);
		$this->email->subject($data['subject']);
		$this->email->message($data['html']);		
		//Send email
		$this->email->send();
		//echo  $this->email->print_debugger();		
	}

}