<?php
//require(APPPATH.'models/Go_model.php');
class Login_model extends CI_model 
{ 
	function __construct() 
	{ 
		//Call the Model constructor 
		parent::__construct(); 
	}

	public function checkLogin($data, &$errormessage)
	{
		$result = array();
		$where_condition = array('email'=>$data['email'],'password'=>$data['password'],'active'=>'1');
		$this->db->select('id,name,email,type');
		$this->db->from('user');
		$this->db->where($where_condition);
		$result = $this->db->get()->row_array();
		//return json
		if(empty($result))
		{
			$errormessage = "Wrong email or password.";
		}
		return $result;
	}

	public function getFireExtinguishers($nfc_id, &$errormessage)
	{
		$result = array();		
		$this->db->select('*');
		$this->db->from('fire_extinguisher');
		$this->db->where('nfc_id',$nfc_id);
		$this->db->where('active', '1');
		$result = $this->db->get()->row_array();
		//return json
		if(empty($result))
		{
			$errormessage = "Record not available";
		}
		return $result;
	}

	public function addServecing($data,$consultant_id,&$errormessage)
	{
		$result = 0;
		$this->db->select('*');
		$this->db->from('fire_extinguisher');
		$this->db->where('nfc_id',$data['nfc_id']);
		$exists = $this->db->get()->row_array();
		
		if(!empty($exists))
		{
			//check user and consultant
			if((Int)$consultant_id == (Int)$exists['consultant_id'])
			{
				$update = [];
	  			$update['who'] = $data['user_id'];
	  			$update['remark'] = $data['remark'];

				//check previous result
				if($exists['result'] !== 'HPT Failed')
				{
					if($exists['result'] == 'US' && $exists['remark'] == 'Empty') 
					{ 
						if(($data['refil'] == '1' || $data['refil'] == '2') && $data['service'] == '0' && $data['htp'] == '0')
						{
							$result = $this->servicing($data,$update,$exists);
						}
						else
						{
							$result = 1;
						}
					}
					else
					{
						$result = $this->servicing($data,$update,$exists);
					}
				}
				else
				{
					//if previous result 'HPT Failed' then no update
					$result = 1;
				}
			}
			else
			{
				//user perform servicing against only that consultant tags (other consultant tags not perform any action)
				$result = 1;
			}			
		}		
		
		if($result == 0)
		{
			$errormessage = "Record not updated";
		}
		return $result;
	}

	public function servicing($data,$update,$exists)
	{
		//get servicing frequency of client
	  			$times = $this->getServicingFrequency($exists['client_id'],$errormessage);
	  			if($times['servicing_frequency'] == '1')
				{
					$month = '1 month';
				}
				if($times['servicing_frequency'] == '2')
				{
					$month = '3 month';
				}

				// get expiry date
				$expdate ='30-'.$exists['expiry_date'];
				$expdate = strtotime($expdate);
				$expdate = date('Y-m-d',$expdate);

				//service
	  			if($data['service'] == '1' || $data['service'] == '2')
	  			{
	  				$action = '1'; 
	  				$update['servicing_date'] = $data['date'];
	  				//if servicing ok
	  				if($data['service'] == '1')
	  				{
	  					$update['result'] = 'OK';
	  					$update['servicing_due'] = date("Y-m-d", strtotime("+".$month, strtotime($data['date'])));
	  				}
	  				//if servicing fail
	  				if($data['service'] == '2')
	  				{
	  					$update['servicing_due'] = 'XX';
	  					//if location changed 
	  					if(strpos($update['remark'], 'OK; Location Changed') !== false)
	  					{
	  						$update['result'] = '';
	  						$update['remark'] = 'OK ('.$data['location'].')';
	  						$update['servicing_due'] = date("Y-m-d", strtotime("+".$month, strtotime($data['date'])));
	  					}
	  					else
	  					{
	  						$update['result'] = 'US';
	  					}  	
	  					//if empty
	  					//echo strpos($update['remark'], 'Empty');exit;
	  					if(strpos($update['remark'], 'Empty') !== false)
	  					{
	  						$update['refill_due'] = 'XX';
	  					}				
	  				}

	  				// if servicing due date greater than expiry date 
	  				// if (strtotime($update['servicing_due']) > strtotime($expdate)) {
	  				// 	$update['servicing_due'] = 'XX';
	  				// }
	  			}

	  			//refill
	  			if($data['refil'] == '1' || $data['refil'] == '2')
	  			{
	  				$action = '2'; 
	  				//get due period
	  				$exp_time = $this->getDuePeriod($exists['type'],$exists['ct_sp'],$errormessage);
	  				$action = '2';
	  				$update['refill_date'] = $data['date'];
	  				$update['servicing_date'] = $data['date'];

	  				$update['refill_due'] = date("Y-m-d", strtotime("+".$exp_time['refill'], strtotime($data['date'])));

	  				$update['servicing_due'] = date("Y-m-d", strtotime("+".$month, strtotime($data['date'])));

	  				if($data['refil'] == '1')
	  				{
	  					$update['result'] = 'Refilled, OK';
	  					$update['ctt'] = $data['ctt'];
	  					$update['cta'] = $data['cta'];
	  				}
	  				if($data['refil'] == '2')
	  				{
	  					$update['result'] = 'Failed';
	  				}

	  				// check activity perform after expiry date
	  				if (strtotime($expdate) < strtotime($update['refill_date'])) {
	  					$update['refill_due'] = 'XX';
	  				}

	  				// if servicing due date greater than expiry date 
	  				// if (strtotime($update['servicing_due']) > strtotime($expdate)) {
	  				// 	$update['servicing_due'] = 'XX';
	  				// }
	  			}
	  			

	  			//hpt
	  			if($data['htp'] == '1' || $data['htp'] == '2')
	  			{
	  				$action = '3'; 
	  				//get due period
	  				$exp_time = $this->getDuePeriod($exists['type'],$exists['ct_sp'],$errormessage);
	  				$update['hpt_date'] = $data['date'];
	  				$update['servicing_date'] = $data['date'];
	  				if($data['htp'] == '1')
	  				{
	  					$update['result'] = 'HPT Passed';
	  					$update['htp_due'] = date("Y-m-d", strtotime("+".$exp_time['hpt'], strtotime($data['date'])));
	  					$update['servicing_due'] = date("Y-m-d", strtotime("+".$month, strtotime($data['date'])));
	  				}
	  				if($data['htp'] == '2')
	  				{
	  					$update['result'] = 'HPT Failed';
	  					$update['servicing_due'] = 'XX';
						$update['refill_date'] = 'XX';
						$update['refill_due'] = 'XX';
						$update['htp_due'] = 'XX';
	  				}

	  				// check activity perform after expiry date
	  				if (strtotime($expdate) < strtotime($update['hpt_date'])) {
	  					$update['htp_due'] = 'XX';
	  				}

	  				// if servicing due date greater than expiry date 
	  				// if (strtotime($update['servicing_due']) > strtotime($expdate)) {
	  				// 	$update['servicing_due'] = 'XX';
	  				// }
	  			} 
  		

	  			//check due dates greater than expiry date
							
				//check refill due date
				if($action == '2' && $update['refill_due'] !== 'XX')
				{
					if($expdate < $update['refill_due'])
					{
						$update['refill_due'] = $expdate;
					} 
				}
				
				//check hpt due date
				if($action == '3' && $update['htp_due'] !== 'XX')
				{
					if($expdate < $update['htp_due'])
					{
						$update['htp_due'] = $expdate;
					} 
				}

				//check servicing due date
				// if($update['servicing_due'] !== 'XX')
				// {
				// 	if($expdate < $update['servicing_due'])
				// 	{
				// 		$update['servicing_due'] = $expdate;
				// 	} 
				// }
				
				
				$this->db->where('nfc_id',$data['nfc_id']);
				$this->db->where('active','1');
				$result = $this->db->update('fire_extinguisher',$update);

				//enter old date into servicing_dates table
				if($result == 1)
				{
					$insert = [];
					$insert['fe_id'] = $exists['id'];
					$insert['fe_no'] = $exists['fe_no'];
					$insert['client_id'] = $exists['client_id'];
					$insert['result'] = $update['result'];
					$insert['type'] = $action;
					//servicing
					if($action == '1')
					{						
						$insert['date'] = $update['servicing_date'];
						if($data['service'] == '2')
						{	
							if($update['remark'] !== '' || $update['remark'] !== null)
        					{
        					    if($update['result'] == '')
        					    {
        					        $insert['result'] = $update['remark'];
        					    }
        					    else
        					    {
        					        $insert['result'] = $insert['result'].' - '.$update['remark'];
        					    }
        						
        					}
						}
						
					}
					//refill
					if($action == '2')
					{
						$insert['date'] = $update['refill_date'];
					}
					//htp
					if($action == '3')
					{
						$insert['date'] = $update['hpt_date'];
					}

					$this->db->insert('history',$insert);	
					// insert into servicing table
					$history_data = $update;
					$history_data['client_id'] = 	$exists['client_id'];
					$history_data['consultant_id'] = 	$exists['consultant_id'];
					$history_data['type'] = 	$exists['type'];
					$history_data['class'] = 	$exists['class'];
					$history_data['ct_sp'] = 	$exists['ct_sp'];
					$history_data['location'] = $exists['location'];
					// $history_data['cta'] = 	$exists['cta'];
					$history_data['capacity'] = 	$exists['capacity'];
					$history_data['mfr'] = 	$exists['mfr'];
					$history_data['expiry_date'] = 	$exists['expiry_date'];
					$history_data['serial'] = 	$exists['serial'];
					$history_data['make'] = 	$exists['make'];
					$history_data['fe_no'] = 	$exists['fe_no']; 
					$history_data['nfc_id'] = $exists['nfc_id'];	
					$history_data['activity'] = $action;
					if($action !== '2')
					{
						$history_data['ctt'] = 	$exists['ctt'];
						$history_data['cta'] = 	$exists['cta'];
					}
					//if location changed 
  					if($update['remark'] == 'OK,Location Changed')
  					{
  						$history_data['location'] = $data['location'];
  					}
					$this->db->insert('servicing_history',$history_data);	
				}

				return $result;
	}

	public function getServicingFrequency($client_id,&$errormessage)
	{
		$result = array();
		$this->db->select('servicing_frequency');
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('id',$client_id);
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}

	public function getDuePeriod($type,$ctsp,&$errormessage)
	{
		$result = array();
		$this->db->select('hpt,refill,life');
		$this->db->from('schedule');
		$this->db->where('type',trim($type));
		$this->db->where('ct_sp',trim($ctsp));
		$result = $this->db->get()->row_array();
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}

	public function getUserConsultant($user_id,&$errormessage)
	{
		$consultant_id = 0;
		$this->db->select('created_by');
		$this->db->from('user');
		$this->db->where('id',trim($user_id));
		$result = $this->db->get()->row_array();
		if(!empty($result))
		{
			$consultant_id = $result['created_by'];
		}
		return $consultant_id;
	}


	public function getAllConsultant(&$errormessage)
	{
		$result = array();
		$this->db->select('id,name,email,phone,address')	;
		$this->db->from('user');
		$this->db->where('active','1');
		$this->db->where('type','2');
		$result = $this->db->get()->result_array();
		if(empty($result))
		{
			$errormessage = "Records not available.";
		}
		return $result;
	}

	public function getAuditorReport($data, &$errormessage)
	{
		$result = array();
		$this->db->select('b.name as consultant_name,a.sr_no,a.type,a.capacity,a.location,a.servicing_date,a.servicing_due, a.refill_date, a.refill_due, a.hpt_date, a.htp_due, a.expiry_date');
		$this->db->from('fire_extinguisher a');
		$this->db->join('user b','a.consultant_id=b.id');
		$this->db->where('a.active','1');
		$this->db->where('a.nfc_id',$data['nfc_id']);
		$result = $this->db->get()->row_array();
		//return json
		if(empty($result))
		{
			$errormessage = "Record not available.";
		}
		return $result;
	}
	
}