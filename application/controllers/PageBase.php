<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

date_default_timezone_set('asia/kolkata');
// require_once './aes-php.php';  

class PageBase 
{
	static $addconsultant_title = 'LWS-Tag';
	// static $logo_url = 'http://localhost:4200/assets/images/lwslogo2.png';
	static $logo_url = 'http://livewireservices.co.in/backend/assets/images/lwslogo2.png';

	static $config = array(
	   'protocol'  => 'smtp',
	   'smtp_host' => 'mail.livewireservices.co.in',
	   'smtp_port' => 587,
	   'smtp_user' => 'info@livewireservices.co.in',
	   'smtp_pass' => '?Xd32fDgSzTD',
	   'mailtype'  => 'html',
	   'charset' => 'iso-8859-1',
	   'wordwrap' => TRUE
  	);


//   	static $config = array(
//   'protocol' => 'smtp',
//   'smtp_host' => 'smtp.mailtrap.io',
//   'smtp_port' => 587,
//   'smtp_user' => 'fac51e656096d4',
//   'smtp_pass' => 'df9670c0f23eb5',
//   'crlf' => "\r\n",
//   'newline' => "\r\n",
//   'mailtype'  => 'html'
// );

	public static function GetLocalDate()
	{
		return new DateTime("now", new DateTimeZone('asia/kolkata'));
	}
	
	public static function convertToDBString($input)
	{
		$input = trim($input);
		return ($input == '' || $input == 'undefined' || $input == 'null') ? NULL : $input;
	}

	//generate random number
	public static function generaterandomno($digits)
	{
		return rand(pow(10, $digits-1), pow(10, $digits)-1);
	}

	public static function generatepassword($length)
	{
		$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
    	return substr(str_shuffle($str_result),  
                       0, $length); 
	}

	public function sendEmail($data)
	{
	//exit;
		$this->load->library('email');
		$this->email->initialize(PageBase::$config);
		// $this->email->set_mailtype("tls");
		$this->email->set_newline("\r\n");
		$this->email->to($data['to']);
		$this->email->from('info@livewireservices.co.in',$data['title']);
		$this->email->subject($data['subject']);
		$this->email->message($data['html']);		
		//Send email
		$this->email->send();
		//echo  $this->email->print_debugger();		
	}

	// get difference between todays date and your date
	public function getDaysDifference($date)
	{		
		$alert = 0; //0- no alert, 1- expiary in week , 2- expired
		$now = time(); 		
		$your_date = strtotime($date);		
		$datediff = $your_date - $now;
		$days = round($datediff / (60 * 60 * 24));
		if($days <= 8 && $days > 0)
		{
			$alert = 1;
		}
		if($days < 0)
		{
			$alert = 2;
		}
		return $alert;
	}

	// check due date greater than expiry date
	public function checkDateGreaterThanExpiryDate($date,$expiry_date)
	{		
		$expired = 0; //0- no expiared, 1- expired
		if(strtotime($date) > strtotime('30-'.$expiry_date))
		{
			$expired = 1;
		}
		return $expired;
	}

}
?>