<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Sample extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->library('excel');
	    $this->load->model('admin/sample_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//admin login
	public function index_post()
	{
		$data['email'] = trim($this->post('email'));
		$data['password'] = md5(trim($this->post('password')));
		$errormessage='';

		$islogin = $this->sample_model->checkLogin($data,$errormessage);
		if(!empty($islogin))
		{
			$json = array("status"=>200,"message"=>'success','logindata'=>$islogin);
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function getconsultant_post()
	{
		$data['begin'] = $this->post('begin');
		$data['user_id'] = trim($this->post('user_id'));
		$errormessage='';

		$result = $this->sample_model->getConsultant($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getConsultant($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','cust_list'=>$result,'totalcount'=>$total);
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function addconsultant_post()
	{
		$data['name'] = trim($this->post('cust_name'));
		$data['address'] = trim($this->post('address'));
		$data['email'] = trim($this->post('email'));
		$data['phone'] = trim($this->post('contact'));
		$data['contact_person'] = trim($this->post('contact_person'));
		$data['created_by'] = trim($this->post('user_id'));
		$data['type'] = '2';
		$password = '123456';
	//	$password = PageBase::generatepassword(6);

		$data['password'] = md5($password);
		$errormessage='';

		$email_exists = $this->sample_model->emailExists($data['email'],$errormessage);

		if($email_exists == 1)
		{
			$result = $this->sample_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$emaildata['to'] = $data['email'];
				$emaildata['title'] = PageBase::$addconsultant_title;
				$emaildata['subject'] = 'Registration';
				$emaildata['html'] = '<div>Hello '.$data['name'].',<br> Your login credentials are:<br>Email:  '.$data['email'].'<br>Password:  '.$password.'</div>';
				PageBase::sendEmail($emaildata);
				$json = array("status"=>200,"message"=>'Record added successfully.');
			}		
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'Email already exists.');
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function updateconsultant_post()
	{
		$data['name'] = trim($this->post('cust_name'));
		$data['address'] = trim($this->post('address'));
		$data['phone'] = trim($this->post('contact'));
		$data['contact_person'] = trim($this->post('contact_person'));
		$record_id = $this->post('record_id');		
		$errormessage='';
		
		$result = $this->sample_model->updateRecord($data,$record_id,'user',$errormessage);
		if($result > 0)
		{			
			$json = array("status"=>200,"message"=>'Record updated successfully.');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function deleteconsultant_post()
	{
		$delete_id = trim($this->post('delete_id'));
		$errormessage='';
		
		$result = $this->sample_model->deleteRecord($delete_id,'user',$errormessage);
		if($result > 0)
		{			
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function getclient_post()
	{
		$data['begin'] = $this->post('begin');
		$data['user_id'] = trim($this->post('user_id'));
		$errormessage='';

		$result = $this->sample_model->getClient($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getClient($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','cust_list'=>$result,'totalcount'=>$total);
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function addclient_post()
	{		
		$data['name'] = trim($this->post('cust_name'));
		$data['address'] = trim($this->post('address'));
		$data['email'] = trim($this->post('email'));
		$data['phone'] = trim($this->post('contact'));
		$data['contact_person'] = trim($this->post('contact_person'));
		$data['servicing_frequency'] = trim($this->post('frequency'));
		$data['created_by'] = trim($this->post('user_id'));
		$data['type'] = '3';
		$password = '123456';
		//$password = PageBase::generatepassword(6);

		$data['password'] = md5($password);
		$errormessage='';

		$email_exists = $this->sample_model->emailExists($data['email'],$errormessage);

		if($email_exists == 1)
		{
			if($_FILES['logofile'] != null && $_FILES['logofile'] != '')
			{
				$tempname = explode(".",$_FILES["logofile"]["name"]); 
				$fileName = uniqid().".".$tempname[1];
				$uploadpath = 'public/images/' . $fileName;
				move_uploaded_file($_FILES['logofile']['tmp_name'], $uploadpath);
				$data['logo_image'] = $uploadpath;
			}

			$result = $this->sample_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$emaildata['to'] = $data['email'];
				$emaildata['title'] = PageBase::$addconsultant_title;
				$emaildata['subject'] = 'Registration';
				$emaildata['html'] = '<div>Hello '.$data['name'].',<br> Your login credentials are:<br>Email:  '.$data['email'].'<br>Password:  '.$password.'</div>';
				PageBase::sendEmail($emaildata);
				$json = array("status"=>200,"message"=>'Record added successfully.');
			}		
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'Email already exists.');
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function updateclient_post()
	{
		$data['name'] = trim($this->post('cust_name'));
		$data['address'] = trim($this->post('address'));
		$data['phone'] = trim($this->post('contact'));
		$data['contact_person'] = trim($this->post('contact_person'));
		$data['servicing_frequency'] = trim($this->post('frequency'));
		$record_id = $this->post('record_id');		
		$errormessage='';
		
		if($_FILES['logofile'] != null && $_FILES['logofile'] != '')
		{  
			$tempname = explode(".",$_FILES["logofile"]["name"]); 
			$fileName = uniqid().".".$tempname[1];
			$uploadpath = 'public/images/' . $fileName;
			move_uploaded_file($_FILES['logofile']['tmp_name'], $uploadpath);
			$data['logo_image'] = $uploadpath;
			$errormessage = "";
			$result = $this->sample_model->updateRecord($data,$record_id,'user',$errormessage);
			if($result > 0)
			{			
				$json = array("status"=>200,"message"=>'Record updated successfully.');
			}		
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$result = $this->sample_model->updateRecord($data,$record_id,'user',$errormessage);
			if($result > 0)
			{			
				$json = array("status"=>200,"message"=>'Record updated successfully.');
			}		
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function deleteclient_post()
	{
		$delete_id = trim($this->post('delete_id'));
		$errormessage='';
		
		$result = $this->sample_model->deleteRecord($delete_id,'user',$errormessage);
		if($result > 0)
		{			
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function getengineer_post()
	{
		$data['begin'] = $this->post('begin');
		$data['user_id'] = trim($this->post('user_id'));
		$errormessage='';

		$result = $this->sample_model->getEngineer($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getEngineer($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','cust_list'=>$result,'totalcount'=>$total);
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function addengineer_post()
	{
		$data['name'] = trim($this->post('cust_name'));
		$data['email'] = trim($this->post('email'));
		$data['phone'] = trim($this->post('contact'));
		$data['created_by'] = trim($this->post('user_id'));
		$data['type'] = '4';
		$password = PageBase::generatepassword(6);

		$data['password'] = md5($password);
		$errormessage='';

		$email_exists = $this->sample_model->emailExists($data['email'],$errormessage);

		if($email_exists == 1)
		{
			$result = $this->sample_model->insertdata($data,'user',$errormessage);
			if($result > 0)
			{
				$emaildata['to'] = $data['email'];
				$emaildata['title'] = PageBase::$addconsultant_title;
				$emaildata['subject'] = 'Registration';
				$emaildata['html'] = '<div>Hello '.$data['name'].',<br> Your login credentials are:<br>Email:  '.$data['email'].'<br>Password:  '.$password.'</div>';
				PageBase::sendEmail($emaildata);
				$json = array("status"=>200,"message"=>'Record added successfully.');
			}		
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'Email already exists.');
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function updateengineer_post()
	{
		$data['name'] = trim($this->post('cust_name'));
		$data['phone'] = trim($this->post('contact'));
		$record_id = $this->post('record_id');		
		$errormessage='';
		
		$result = $this->sample_model->updateRecord($data,$record_id,'user',$errormessage);
		if($result > 0)
		{			
			$json = array("status"=>200,"message"=>'Record updated successfully.');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function deleteengineer_post()
	{
		$delete_id = trim($this->post('delete_id'));
		$errormessage='';
		
		$result = $this->sample_model->deleteRecord($delete_id,'user',$errormessage);
		if($result > 0)
		{			
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function uploadexcel_post()
	{
		$user_id = $this->post('user_id');
		$consultant_id = $this->post('consultant_id');
		$client_id = $this->post('client_id');
		
		if(isset($_FILES['excelfile']['name']))
		{
			$inputFileName = $_FILES['excelfile']['tmp_name'];
			try 
			{
			    $inputFileType = PHPExcel_IOFactory::identify($inputFileName); 
			    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
			    $objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
			    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			    . '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			//  Loop through each row of the worksheet in turn
			$resErr = array();
			$resSuc = array();			

			for ($row = 2; $row <= $highestRow; $row++)
			{
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);					
				if($rowData != NULL && $rowData != '' )
				{				
					$flag = 0;
				    $continue = TRUE;
				    $columName = array();
				    $isDataAvailable = array();	
				 //    if($highestColumn != 'M')
					// {
					// 	$columName[] = 'Please check columns';	
					// 	$continue = false;				
					// }
				    foreach($rowData[0] as $colindex=>$columndata)
				    {
				    	
				    	if($colindex == 0 && ($columndata == '' || $columndata == null))
				    	{
							$continue = false;
							$columName[] = "FE.No. is required";
						}
					
						if($colindex == 10)
						{
							$pattern = '/^([0-9]{1,2})\\-([0-9]{4})$/';
    						if(preg_match($pattern, $columndata) == 0)
    						{
    							$continue = false;
    							$columName[] = "Check manufacturing date format.";
    						}	
						}
						
						if($columndata != '' || $columndata != null)
						{
							$isDataAvailable[] = 'yes';
						}
				    }

				  	$data['list']= $rowData;
				  	$data['list']= $rowData;
				  	$data['user_id'] = $user_id;
				  	$data['consultant_id'] = $consultant_id;
				  	$data['client_id'] = $client_id;
				  	//generate alphanumeric string
				  	$data['nfc_id'] = PageBase::generatepassword(8);
				    if($continue)
				    {
				   		$errormessage = '';
				   		$options = array();
				   		//generate sr no
				   		$data['sr_no'] = $this->sample_model->getLastSrno($data['client_id'], $errormessage);

				   		//get servicing frequency of client
	  					$feq = $this->sample_model->getServicingFrequency($client_id,$errormessage);
	  					if($feq['servicing_frequency'] == '1')
						{
							$month = '1 month';
						}
						if($feq['servicing_frequency'] == '2')
						{
							$month = '3 month';
						}	

				   		//get expiry periods  
				   		$expiry = $this->sample_model->getDuePeriod($data['list'][0][2],$data['list'][0][3],$errormessage);	
				   					   	
				   		$data['expiry'] = $expiry['life'];

				   		//set hpt date
				   		if($data['list'][0][11] !== '' && $data['list'][0][11] !== null)
				   		{
				   			//$data['hpt_date'] = $data['list'][0][11];
				   			$hp_dt = explode('-', $data['list'][0][11]);
				   			$data['hpt_date']=$hp_dt[1].'-'.$hp_dt[0].'-01 00:00:00';
				   		}
				   		else
				   		{
				   			//if hpt date '' then set mfr date as hpt date
				   			$mfr = explode('-', $data['list'][0][10]);
				   			$data['hpt_date']=$mfr[1].'-'.$mfr[0].'-01 00:00:00';
				   		}				   		
				   	
				   		
				   		//set hpt due
			   			$data['hpt_due'] = date("Y-m-d", strtotime("+".$expiry['hpt'], strtotime($data['hpt_date'])));

			   			//set refill date
				   		if($data['list'][0][12] !== '' && $data['list'][0][12] !== null)
				   		{
				   			//$data['refill_date'] = $data['list'][0][12];
				   			$ref_dt = explode('-', $data['list'][0][12]);
				   			$data['refill_date']=$ref_dt[1].'-'.$ref_dt[0].'-01 00:00:00';				   			
				   		}
				   		else
				   		{
				   			//if refill date '' then set mfr date as refill date
				   			$mfr = explode('-', $data['list'][0][10]);
				   			$data['refill_date']=$mfr[1].'-'.$mfr[0].'-01 00:00:00';
				   		}

				   		//set refill due
			   			$data['refill_due'] = date("Y-m-d", strtotime("+".$expiry['refill'], strtotime($data['refill_date'])));

			   			//set servicing date
			   			$data['servicing_date'] = date('Y-m-d h:i:s',strtotime($data['list'][0][13]));
			   		    
			   			
						$userid = $this->sample_model->UploadExcel($data, $errormessage);												
						$valid = ((int)$userid > 0);		  
						if(!$valid)
						{
							$inserterrors = array();
							$inserterrors[] = $row;
							$inserterrors[] = $errormessage;
							$resErr[] = $inserterrors;
						}
						else
						{
							$resSuc[] = $row;
						}
						
				    }
					else
					{
						$flag = 1;
						$errors[] = $row;
						$errors[] = " ".(implode(",",$columName))." ";
					}
					if($flag == 1)
					{
						// Error Comes
						if(count($errors)>0)
						{
							$resErr[] = $errors;
							$errors = array();
							continue;		
						}				
					}	
				}				
			}
		}

		$res = array();
		if($highestRow == 2)
		{
			$res[] = 'Please first add data into downloaded excel file and then upload here';					
		}
		if(!empty($resErr))
		{
			$excelArr = array();
			for($i=0;$i< count($resErr);$i++)
			{
				$row = $resErr[$i][0];
				$tempArr = array();
				$tempArr[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				$tempArr[] = $resErr[$i][1];
				$excelArr[] = $tempArr;
			}
			
			$finalRes['errorexcel'] = "";
			$finalRes['error'] = $resErr;
		}
		else
		{
			//$finalRes['emptyerror'] = $res;
			$finalRes['error'] = 0;
		}
		$finalRes['success'] = count($resSuc);
		
	
		// print_r($finalRes);
		// exit;
		 $json = array("status"=>200,"messagearr"=>$finalRes);
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function getadminreport_post()
	{
		$data['begin'] = $this->post('begin');
		$data['admin_id'] = trim($this->post('admin_id'));
		$data['consultant_id'] = trim($this->post('consultant_id'));
		$data['client_id'] = trim($this->post('client_id'));
		$data['year'] = trim($this->post('year'));
		$data['month'] = trim($this->post('month'));
		$errormessage='';

		$result = $this->sample_model->getAdminReport($data,$errormessage);
	
		if($result > 0)
		{	
			$data['begin'] = '';		
			$total = count($this->sample_model->getAdminReport($data,$errormessage));

			for($i=0;$i<count($result);$i++) 
			{					
					//set remark value
					if($result[$i]['remark'] == NULL || $result[$i]['remark'] == '')
					{
						$result[$i]['remark'] = '';
					}
					else
					{
						if($result[$i]['result'] == '')
						{
							$result[$i]['remark'] = $result[$i]['remark'];
						}
						else
						{
							$result[$i]['remark'] = ' - '.$result[$i]['remark'];
						}						
					}
					
					
					$result[$i]['representative_name'] = '';
					//get representative name
					if($result[$i]['who'] !== '' && $result[$i]['who'] !== NULL)
					{						
						$re_name = $this->sample_model->getRepresentativeName($result[$i]['who'],$errormessage);
						$representive_name = explode(' ', $re_name['name']);
						
						$result[$i]['representative_name'] = $representive_name[0];
					}

					//get servicing due date
					if($result[$i]['servicing_due'] == 'XX')
					{
						$result[$i]['servicing_alert'] = 0;
					}
					else
					{
						if($result[$i]['servicing_due'] !== '' && $result[$i]['servicing_due'] !== null)
						{						
							//find expiary date difference
							$result[$i]['servicing_alert'] = PageBase::getDaysDifference($result[$i]['servicing_due']);
							$result[$i]['servicing_due'] = date("d-m-Y", strtotime($result[$i]['servicing_due']));
						}
					}


					//refill dates
					if($result[$i]['refill_due'] == 'XX')
					{
						$result[$i]['refill_alert'] = 0;
					}
					else
					{
						//find expiary date difference
						$result[$i]['refill_alert'] = PageBase::getDaysDifference($result[$i]['refill_due']);
						$result[$i]['refill_due'] = date("m-Y", strtotime($result[$i]['refill_due']));
					}

					//hpt dates
					if($result[$i]['htp_due'] == 'XX')
					{
						$result[$i]['hpt_alert'] = 0;
					}
					else
					{
						//find expiary date difference
						$result[$i]['hpt_alert'] = PageBase::getDaysDifference($result[$i]['htp_due']);
						$result[$i]['htp_due'] = date("m-Y", strtotime($result[$i]['htp_due']));
					}

					//change servicing date format
					$result[$i]['servicing_date'] = date("d-m-Y h:i", strtotime($result[$i]['servicing_date']));
					//change refill date format
					if($result[$i]['refill_date'] !== 'XX')
					{
					    if($result[$i]['refill_date'] !== NULL)
					    {
						    $result[$i]['refill_date'] = date("m-Y", strtotime($result[$i]['refill_date']));
					    }
					    else
					    {
					        $result[$i]['refill_date'] = '';
					        $result[$i]['refill_due'] = '';
					    }
					}
					//change hpt date format
					if($result[$i]['hpt_date'] !== 'XX')
					{
						$result[$i]['hpt_date'] = date("m-Y", strtotime($result[$i]['hpt_date']));
					}
				}



			$json = array("status"=>200,"message"=>'success','report' => $result,'totalcount' =>$total);
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function updatefireextinguisher_post()
	{
		$editid = $this->post('editid');
		$data['location'] = trim($this->post('location'));
		$data['ctt'] = trim($this->post('ctt'));
		$data['cta'] = trim($this->post('cta'));
		$errormessage='';
		
		$result = $this->sample_model->updateFireExtinguisher($data,$editid,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'Record updated successfully.');
		}				
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function deletefireextinguisher_post()
	{
		$recordid = $this->post('recordid');
		$data['active'] = '0';
		$errormessage='';
		
		$result = $this->sample_model->updateFireExtinguisher($data, $recordid,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'Record deleted successfully.');
		}				
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function getnfctagreport_post()
	{
		$data['begin'] = $this->post('begin');
		$data['admin_id'] = trim($this->post('admin_id'));
		$data['consultant_id'] = trim($this->post('consultant_id'));
		$data['client_id'] = trim($this->post('client_id'));
		$errormessage='';
		
		$result = $this->sample_model->getNFCTagReport($data,$errormessage);
		if(!empty($result))
		{
			$data['begin'] = '';
			$total = count($this->sample_model->getNFCTagReport($data,$errormessage));
			$json = array("status"=>200,"message"=>'success','report' => $result,'totalcount' =>$total);
		}				
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	public function gethistoryreport_post()
	{
		$data['begin'] = $this->post('begin');
		$data['admin_id'] = trim($this->post('admin_id'));
		$data['consultant_id'] = trim($this->post('consultant_id'));
		$data['client_id'] = trim($this->post('client_id'));
		$data['fe_no'] = trim($this->post('fe_no'));
		$data['year'] = trim($this->post('year'));
		$errormessage='';

		//get fe detail
		$detail = $this->sample_model->getFEDetail($data,$errormessage);
		if(!empty($detail))
		{
			$result = $this->sample_model->getHistoryReport($data,$errormessage);
			if(!empty($result))
			{
				$data['begin'] = '';
				$total = count($this->sample_model->getHistoryReport($data,$errormessage));
				$json = array("status"=>200,"message"=>'success','report' => $result,
								'totalcount' =>$total, 'fe_detail' =>$detail);
			}				
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'FE No not available');
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//Change Password
	public function changepassword_post()
	{
		$data['oldpass'] = trim($this->post('oldpass'));
		$data['newpass'] = trim($this->post('newpass'));
		$data['confirm_pass'] = trim($this->post('confirm_pass'));
		$data['user_id'] = $this->post('user_id');
		$errormessage = '';

		if($data['newpass'] == $data['confirm_pass'])
		{
			$result = $this->sample_model->changePassword($data,$errormessage);			
			if($result == 1)
			{
				$json = array("status"=>200,"message"=>'Password changed successfully.',);
			}
			else
			{
				$json = array("status"=>400,"message"=>$errormessage);
			}
		}
		else
		{
			$json = array("status"=>400,"message"=>'New password and confirm password not match.');
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}

	//cron job
	public function sendnotification_get()
	{
		$result = $this->sample_model->getexpirynotifications($errormessage);			
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'success');
		}
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
}