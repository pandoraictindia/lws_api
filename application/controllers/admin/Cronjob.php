<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'controllers/PageBase.php');
require(APPPATH.'libraries/Format.php');
require(APPPATH.'libraries/REST_Controller.php');
class Cronjob extends REST_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	    $this->load->library('excel');
	    $this->load->model('admin/cronjob_model');
	}

	public function _remap($method, $param)
	{
		$type = strtolower($_SERVER['REQUEST_METHOD']);	
		$method = $method."_".$type;
		if (method_exists($this, $method))
		{
			return $this->$method($param);
		}
		else
		{
			$this->load->view('pagenotfound',null);
		}
	}

	//cron job api
	public function index_get()
	{
		$errormessage='';

		$result = $this->cronjob_model->getConsultantRemider(PageBase::$logo_url,$errormessage);
		if($result == 1)
		{
			$json = array("status"=>200,"message"=>'success');
		}		
		else
		{
			$json = array("status"=>400,"message"=>$errormessage);
		}

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	public function crontest_get()
	{
		$data['title'] = 'test';
		$data['subject'] = 'Test';
		$data['to'] = 'jyotipawar38@gmail.com';
		//$data['cc'] = array('jyotiingavale78@gmail.com','siddhitest12@gmail.com');
		$data['cc'] = 'siddhitest12@gmail.com';
		$data['html'] = 'Hello';
		$result = $this->cronjob_model->sendEmail($data);
		$json = array("status"=>200,"message"=>'success');

		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		echo json_encode($json);
	}
	
	public function emailtest_get()
	{
		//cron job email
		// $data['title'] = 'test';
		// $data['subject'] = 'Test';
		// $data['to'] = 'jyotipawar38@gmail.com';
		// $data['cc'] = 'jyotiingavale78@gmail.com';
		// $data['html'] = 'Hello';
		// $result = $this->cronjob_model->sendEmail($data);

		// add engineer email
		$emaildata['to'] = 'jyotipawar38@gmail.com';
		$emaildata['title'] = PageBase::$addconsultant_title;
		$emaildata['subject'] = 'Registration';
		$emaildata['html'] = '<div>Hello jyoti,<br><br> Your login credentials are:<br><br>Email:  jyotipawar38@gmail.com<br>Password:  123456</div><br><br>';
		$emaildata['html'] .= '<div>Thanks and Regards,</div>';
// 		$emaildata['html'] .= '<div>LWS - Tags (STRAP)<br>
// 			<img src=" '.PageBase::$logo_url.' "  width="100" border="0" alt="" style="display: block;"><br>
// 			<a href="http://livewireservices.co.in">http://livewireservices.co.in</a></div>';
		PageBase::sendEmail($emaildata);
		exit;
	}
}