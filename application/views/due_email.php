<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Emailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

	<style>
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	}
	
	table td{
	    text-align:center;
	}
	</style>
</head>
<body>

	<div style="font-family: calibri; margin: 0 auto;border:1px solid #ccc; padding:20px; border-radius: 5px">

		<p>Hello Welcome to <b><?php echo $detail['consultant_name']; ?>,</b></p>

		<p>&nbsp;</p>

		<p>The following Fire extinguishers at <?php echo $detail['name']; ?> are due for refilling / HPT / Expiry. Kindly note the same and take necessary action.</p>	
	
		
		<table>
			
			<tr>
				<th rowspan="2">Sr.No.</th>
				<th rowspan="2">Type</th>
				<th rowspan="2">CT /<br>SP</th>
				<th rowspan="2">Class</th>
				<th rowspan="2">Capacity<br>(ltr / kg)</th>
				<th colspan="2">Refilling</th>
				<th colspan="2">HPT</th>
				<th colspan="2">Life</th>
			</tr>

			<tr>				
				<th>Sr. Nos</th>
    			<th>Qty (nos)</th>
    			<th>Sr. Nos</th>
    			<th>Qty (nos)</th>
    			<th>Sr. Nos</th>
    			<th>Qty (nos)</th>
			</tr>

			<tr>
				<th colspan="11" style="text-align:left;">
					<span style="font-weight:bold;"><?php echo date('M Y'); 
					if(empty($detail['month1']))  {
						echo ' - Nil';
					}
					?>
						
					</span>
				</th>
			</tr>



			<?php 
				for($i=0;$i<count($detail['month1']);$i++) {
			?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $detail['month1'][$i]['type']?></td>
				<td><?php echo $detail['month1'][$i]['ct_sp']?></td>
				<td><?php echo $detail['month1'][$i]['class']?></td>
				<td><?php echo $detail['month1'][$i]['capacity']?></td>
				<td>
					<?php if($detail['month1'][$i]['refill_count'] == 0) {
						echo '-';
						} else {echo $detail['month1'][$i]['refill_srno'];}?>						
				</td>
				<td><?php echo $detail['month1'][$i]['refill_count']?></td>
				<td>
					<?php if($detail['month1'][$i]['hpt_count'] == 0) {
						echo '-';
						} else {echo $detail['month1'][$i]['hpt_srno'];}?>
				</td>
				<td><?php echo $detail['month1'][$i]['hpt_count']?></td>
				<td>
					<?php if($detail['month1'][$i]['expiry_count'] == 0) {
						echo '-';
						} else {echo $detail['month1'][$i]['expiry_srno'];}?>	
				</td>
				<td><?php echo $detail['month1'][$i]['expiry_count']?></td>
			</tr>

			<?php 
				}
			?>



			<tr>
				<th colspan="11" style="text-align:left;">
					<span style="font-weight:bold;">
						<?php echo date('M Y', strtotime('+1 month'));
						if(empty($detail['month2']))  {
							echo ' - Nil';
						}
						?>
						
					</span>
				</th>
			</tr>

			<?php 
				for($i=0;$i<count($detail['month2']);$i++) {
			?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $detail['month2'][$i]['type']?></td>
				<td><?php echo $detail['month2'][$i]['ct_sp']?></td>
				<td><?php echo $detail['month2'][$i]['class']?></td>
				<td><?php echo $detail['month2'][$i]['capacity']?></td>
				<td>
					<?php if($detail['month2'][$i]['refill_count'] == 0) {
						echo '-';
						} else {echo $detail['month2'][$i]['refill_srno'];}?>						
				</td>
				<td><?php echo $detail['month2'][$i]['refill_count']?></td>
				<td>
					<?php if($detail['month2'][$i]['hpt_count'] == 0) {
						echo '-';
						} else {echo $detail['month2'][$i]['hpt_srno'];}?>
				</td>
				<td><?php echo $detail['month2'][$i]['hpt_count']?></td>
				<td>
					<?php if($detail['month2'][$i]['expiry_count'] == 0) {
						echo '-';
						} else {echo $detail['month2'][$i]['expiry_srno'];}?>	
				</td>
				<td><?php echo $detail['month2'][$i]['expiry_count']?></td>
			</tr>

			<?php 
				}
			?>



			<tr>
				<th colspan="11" style="text-align:left;">
					<span style="font-weight:bold;">
						<?php echo date('M Y', strtotime('+2 month'));
						if(empty($detail['month3']))  {
							echo ' - Nil';
						}
						?>						
					</span>
				</th>
			</tr>

			<?php 
				for($i=0;$i<count($detail['month3']);$i++) {
			?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $detail['month3'][$i]['type']?></td>
				<td><?php echo $detail['month3'][$i]['ct_sp']?></td>
				<td><?php echo $detail['month3'][$i]['class']?></td>
				<td><?php echo $detail['month3'][$i]['capacity']?></td>
				<td>
					<?php if($detail['month3'][$i]['refill_count'] == 0) {
						echo '-';
						} else {echo $detail['month3'][$i]['refill_srno'];}?>						
				</td>
				<td><?php echo $detail['month3'][$i]['refill_count']?></td>
				<td>
					<?php if($detail['month3'][$i]['hpt_count'] == 0) {
						echo '-';
						} else {echo $detail['month3'][$i]['hpt_srno'];}?>
				</td>
				<td><?php echo $detail['month3'][$i]['hpt_count']?></td>
				<td>
					<?php if($detail['month3'][$i]['expiry_count'] == 0) {
						echo '-';
						} else {echo $detail['month3'][$i]['expiry_srno'];}?>	
				</td>
				<td><?php echo $detail['month3'][$i]['expiry_count']?></td>
			</tr>

			<?php 
				}
			?>

		</table>

		<p style="font-weight: 600;padding-top:30px">Thanks & Regards,</p>
	
		<p>
			LWS - Tags (STRAP)<br>
			<img src="<?php echo $detail['logo_url']; ?>"  width="100" border="0" alt="" style="display: block;"><br>
			<a href="http://livewireservices.co.in">http://livewireservices.co.in</a>
		</p>

	</div>
</body>
</html>

