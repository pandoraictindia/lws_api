<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Emailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: calibri; margin: 0 auto;border:1px solid #ccc; padding:20px;
border-radius: 5px">
	
	
	<tr>
		<td colspan="2">Hello Welcome to <b><?php echo $detail['consultant_name']; ?>,</b></td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
	
	<!--<tr><td colspan="2"><h2>Welcome to Livewire Services</h2></td></tr>-->

	<tr>
		<td colspan="2">
			The following Fire extinguishers at <?php echo $detail['name']; ?> are due for refilling / HPT / Expiry. Kindly note the same and take necessary action.
		</td>
	</tr>

	<tr><td>&nbsp;</td></tr>

	<tr>
		<td><span style="text-decoration:underline;font-weight:bold;"><?php echo date('M Y'); ?></span></td>
	</tr>	
	
	<tr>
		<td><b>Refill Due:-</b></td>
		
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['refill_due1'] !== '') {
	    			echo $detail['refill_due1']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	    	
	    </td>
	</tr>	
	
	<tr>
		<td width="20%"><b>HPT Due:-</b></td>	
	</tr>
	
	<tr>
	    <td>Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['hpt_due1'] !== '') {
	    			echo $detail['hpt_due1']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	
	    </td>
	</tr>	

	<tr>
		<td><b>Expiry:-</b></td>
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['expiry_due1'] !== '') {
	    			echo $detail['expiry_due1']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	    	
	    </td>
	</tr>

	
	<tr><td>&nbsp;</td></tr>


	<tr>
		<td><span style="text-decoration:underline;font-weight:bold;"><?php echo date('M Y', strtotime('+1 month')) ?></span></td>
	</tr>	
	
	<tr>
		<td><b>Refill Due:-</b></td>
		
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;	     
	    	<?php 
	    		if($detail['refill_due2'] !== '') {
	    			echo $detail['refill_due2']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	
	    </td>
	</tr>	
	
	<tr>
		<td width="20%"><b>HPT Due:-</b></td>
	
	</tr>
	
	<tr>
	    <td>Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['hpt_due2'] !== '') {
	    			echo $detail['hpt_due2']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>
	    </td>
	</tr>	

	<tr>
		<td><b>Expiry:-</b></td>
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;    
	    	<?php 
	    		if($detail['expiry_due2'] !== '') {
	    			echo $detail['expiry_due2']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	
	    </td>
	</tr>	


	
	<tr><td>&nbsp;</td></tr>



	<tr>
		<td><span style="text-decoration:underline;font-weight:bold;"><?php echo date('M Y', strtotime('+2 month')) ?></span></td>
	</tr>    
	
	<tr>
		<td><b>Refill Due:-</b></td>
		
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['refill_due3'] !== '') {
	    			echo $detail['refill_due3']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	 	
	    </td>
	</tr>	
	
	<tr>
		<td width="20%"><b>HPT Due:-</b></td>
	
	</tr>
	
	<tr>
	    <td>Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['hpt_due3'] !== '') {
	    			echo $detail['hpt_due3']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	
	    </td>
	</tr>    

    <tr>
		<td><b>Expiry:-</b></td>
	</tr>
	
	<tr>
	    <td  colspan="2">Fire Extinguisher Sr.No.&nbsp;
	    	<?php 
	    		if($detail['expiry_due3'] !== '') {
	    			echo $detail['expiry_due3']; 
	    		} else {
	    			echo 'Nil';
	    		}
	    	?>	
	    </td>
	</tr>	

	
	<tr><td colspan="2" style="font-weight: 600;padding-top:30px">Thanks & Regards,</td></tr>
	
	<tr>
		<td colspan="2">
			LWS - Tags (STRAP)<br>
			<img src="<?php echo $detail['logo_url']; ?>"  width="100" border="0" alt="" style="display: block;"><br>
			<a href="http://livewireservices.co.in">http://livewireservices.co.in</a>
		</td>
	</tr>
</table>
</body>
</html>

