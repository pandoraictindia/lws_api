<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Emailer</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

	<style>
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	}
	</style>
</head>
<body>

	<div style="font-family: calibri; margin: 0 auto;border:1px solid #ccc; padding:20px; border-radius: 5px">

		<p>Hello <b><?php echo $detail['user_name']; ?>,</b></p>


		<p><h2>Welcome to LWS-Tag</h2></p>	
	
		<p>You recently requested to your password for your LWS-Tag account.</p>

		<p>Please login to your account using following details:</p>

		<p>Email: <?php echo $detail['email']; ?></p>

		<p>Password: <?php echo $detail['password']; ?></p>
		

		<p style="font-weight: 600;padding-top:30px">Thanks & Regards,</p>
	
		<p>
			LWS - Tags (STRAP)<br>
			<img src="<?php echo $detail['logo_url']; ?>"  width="100" border="0" alt="" style="display: block;"><br>
			<a href="http://livewireservices.co.in">http://livewireservices.co.in</a>
		</p>

	</div>
</body>
</html>

